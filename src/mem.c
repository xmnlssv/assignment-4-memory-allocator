#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool is_block_sufficient(size_t required_size, struct block_header* blk) {
    return blk->capacity.bytes >= required_size;
}
static size_t calculate_pages_needed(size_t memory) {
    size_t page_size = getpagesize();
    return (memory / page_size) + ((memory % page_size) > 0);
}
static size_t          round_pages   ( size_t memory )                      { return getpagesize() * calculate_pages_needed( memory ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
    const size_t region_size = region_actual_size(size_from_capacity((block_capacity){query}).bytes);
    void* region_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (region_addr == MAP_FAILED) {
        region_addr = map_pages(addr, region_size, 0x0);
        if (region_addr == MAP_FAILED)
            return REGION_INVALID;
    }

    block_init(region_addr, (block_size){region_size}, NULL);
    return (struct region) {
        .addr = region_addr,
        .size = region_size,
        .extends = (region_addr == addr)
    };
}

static void* next_block_address(const struct block_header* current_block);

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

static bool blocks_continuous(struct block_header const* fst, struct block_header const* snd );

/*  освободить всю память, выделенную под кучу */
void heap_term() {
    struct block_header* start = (struct block_header*)HEAP_START;
    struct block_header* current = start;
    size_t region_size = 0;

    while (current != NULL) {
        region_size += size_from_capacity(current->capacity).bytes;

        // Если следующий блок существует и блоки непрерывны, просто переходим к следующему блоку
        if (current->next != NULL && blocks_continuous(current, current->next)) {
            current = current->next;
        } else {
            // В противном случае, освобождаем регион и переходим к следующему стартовому блоку
            struct block_header* next = current->next;
            munmap(start, round_pages(region_size));
            start = next;
            current = next;
            region_size = 0;
        }
    }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (block_splittable(block, query)) {
      void* new_block = block->contents + query;
      const block_size new_size = (block_size){.bytes = (block->capacity.bytes - query)};

      block_init(new_block, new_size, block->next);
      block->next = new_block;
      block->capacity.bytes = query;

      return true;
  }

  return false;
}


/*  --- Слияние соседних свободных блоков --- */

static void* next_block_address(const struct block_header* current_block) {
    return (void*)(current_block->contents + current_block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == next_block_address(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next(struct block_header* current_block) {
    if (current_block->next == NULL)
        return false; 

    bool can_merge_blocks = mergeable(current_block, current_block->next);
    
    if (!can_merge_blocks)
        return false;

    current_block->capacity.bytes += size_from_capacity(current_block->next->capacity).bytes;
    current_block->next = current_block->next->next;

    return true;
}


/*  --- ... если размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  if (block == NULL)
      return (struct block_search_result){BSR_CORRUPTED, block};

  while (block->next != NULL) {
      while (try_merge_with_next(block));

      if (block->is_free && is_block_sufficient(sz, block)) {
          return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
      }

      block = block->next;
  }

  if (block->is_free && is_block_sufficient(sz, block)) {
        return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
  }

  return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, block};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  size_t size = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = find_good_or_last(block, size);

  if(result.type == BSR_FOUND_GOOD_BLOCK) {
      split_if_too_big(result.block, query);
      result.block->is_free = false;
  }

  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if (last == NULL)
      return NULL;

  size_t size = size_max(query, BLOCK_MIN_CAPACITY);
  void* addr = next_block_address(last);
  struct region new_region = alloc_region(addr, size);

  if (new_region.addr == NULL)
      return NULL;

  last->next = new_region.addr;

  if (try_merge_with_next(last))
      return last;

  return new_region.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  size_t size = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_search_result result = try_memalloc_existing(size, heap_start);

  if (result.type == BSR_CORRUPTED) {
      return NULL;
  } else if (result.type == BSR_REACHED_END_NOT_FOUND) {
      result = try_memalloc_existing(size, grow_heap(result.block, size));
      if (result.type != BSR_FOUND_GOOD_BLOCK) return NULL;
  }

  return result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;

  while (try_merge_with_next(header));
}
